# IMPORTS

# Para trabajar con la función raiz cuadrada de Math
from math import sqrt
# Para trabajar con fechas y horas
import time
# Para comunicacion cliente servidor
import socket
# Para trabajar con urls
import urllib

#----------------------------------------------------------------------
# GLOBALES

# A los nodos que hemos creado les hemos asignado un nombre.
# Aquí los guardamos en variables

# Los 2 objetos que muestran las herramientas y menús que se controlan con las manos.
# controller0: izquierda
leftNode = findNode("Botonera")

# controller1: derecha
clipNode1 = findNode("Figuras")

#----------------------------------------------------------------------
# Función recursiva que esta a la espera de eventos
# HTC VIVE - Interacción con objetos virtuales - Controladores
#----------------------------------------------------------------------

# Sensor táctil

def triggerSensor( node):
    if node.hasAttachment("TouchSensorAttachment"):
        touchAttachment = node.getAttachment("TouchSensorAttachment")
        acc = vrFieldAccess(touchAttachment)
        if acc.isValid():
            variantSetName = acc.getMString("variantSets")
            for vset in variantSetName:
                selectVariantSet(vset)
                controller1.triggerHapticPulse(0,1000)
        return
    parentNode = node.getParent()
    
    if parentNode.isValid():
        # Recursividad (función que se llama a si misma)
        triggerSensor( parentNode)


#----------------------------------------------------------------------

# Funciones de HTC VIVE que se lanzan al ocurrir los diferentes eventos dados

def trigger0Pressed():
    # Eje que activa este controlador (de 0 a 3)
    controller0.setPickingAxis(0)

def trigger0Released():
    controller0.showPickingAxis(false)

def grip0Pressed():
    selectNode(getSelectedNode(), false)

def grip0Released():
    print("grip0Released")

def touchpad0Pressed():
    print("touchpad0Pressed")

def touchpad0Released():
    print("touchpad0Released")

def touchpad0PositionChanged(position):
    print("touchpad0PositionChanged")

def controller0Moved():
    leftNode.setTransformMatrix( controller0.getWorldMatrix(), false)
    touchPos = controller0.getTouchpadPosition()

def trigger1Pressed():
    controller1.setPickingAxis(0)
    controller1.showPickingAxis(true)

def trigger1Released():
    pickedNode = controller1.pickNode()
    controller1.showPickingAxis(false)
    triggerSensor(pickedNode)

def grip1Pressed():
    setOpenVRTrackingOrigin( Pnt3f(0.0, 0.0, 0.0))

def touchpad1PositionChanged(position):
    print("touchpad1PositionChanged")

def getNormalizedDirection(matrix):
    inVec = Vec2f( -matrix[2], matrix[6])
    rescale = 1.0 / sqrt( inVec.x()*inVec.x() + inVec.y() * inVec.y())
    return Vec2f( inVec.x() * rescale, inVec.y() * rescale)

def controller1Moved():
    print("controller1Moved")
    

#----------------------------------------------------------------------

# Se crean dos controladores y se conecta su señal a las funciones según sea el evento

# Instancia de vrOpenVRController
controller0 = vrOpenVRController("Controller0")
# Cuando se producen esos eventos se disparan funciones
controller0.connectSignal("controllerMoved", controller0Moved)
controller0.connectSignal("triggerPressed", trigger0Pressed)
controller0.connectSignal("triggerReleased", trigger0Released)
controller0.connectSignal("gripPressed", grip0Pressed)
controller0.connectSignal("gripReleased", grip0Released)
controller0.connectSignal("touchpadPressed", touchpad0Pressed)
controller0.connectSignal("touchpadReleased", touchpad0Released)
#controller0.connectSignal("touchpadPositionChanged", touchpad0PositionChanged)

# Instancia de vrOpenVRController
controller1 = vrOpenVRController("Controller1")
# Cuando se producen esos eventos se disparan funciones
controller1.connectSignal("controllerMoved", controller1Moved)
controller1.connectSignal("triggerPressed", trigger1Pressed)
controller1.connectSignal("triggerReleased", trigger1Released)
controller1.connectSignal("gripPressed", grip1Pressed)
controller1.connectSignal("gripReleased", grip1Released)
controller1.connectSignal("touchpadPressed", touchpad1Pressed)
controller1.connectSignal("touchpadReleased", touchpad1Released)
#controller1.connectSignal("touchpadPositionChanged", touchpad1PositionChanged)

#----------------------------------------------------------------------
# En caso de que el origen de la escena no se encuentre en 0,0,0, se establece el origen de referencia.
# Pnt3f es una estructura de datos para geometría 3d
setOpenVRTrackingOrigin( Pnt3f(0.0, 0.0, 0.0))

#---------------------------------------------------------------------
# INICIALIZAR / RESETEAR
def resetScene():

    print "reset"
    
    # Desactivar plano de recorte
    enableClippingPlane(false)
    
    # habilita botón
    selectVariantSet("boton1")
    print "reset end"
    
    # Desactivo checkbox
    clipNode1.fields().setBool("on",false)


# Iniciar
resetScene()

# Ccnfigura la transformación de la cámara activa
# Valores predeterminados
setFromAtUp(-1, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0)

